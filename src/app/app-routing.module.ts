import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {RunnersComponent} from './pages/runners-list/runners.component';
import {PreferencesComponent} from './pages/preferences/preferences.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'runners', component: RunnersComponent },
  { path: 'preferences', component: PreferencesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
