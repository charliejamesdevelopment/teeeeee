import {FormBuilder, Validators} from '@angular/forms';

export function createPreferencesFormGroup(formBuilder: FormBuilder): any {
  return formBuilder.group({
    title: [, [Validators.required]],
    name: [, [Validators.required]],
    email: [, [Validators.required]],
    company: [],
    settings: formBuilder.array([])
  });
}

export function createSettingFormGroup(formBuilder: FormBuilder): any {
  return formBuilder.group({
    id: [],
    settingDescription: [],
    value: []
  });
}
