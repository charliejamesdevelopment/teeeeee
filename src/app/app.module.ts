import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  NbThemeModule,
  NbLayoutModule,
  NbMenuModule,
  NbUserModule,
  NbInputModule,
  NbFormFieldModule,
  NbCardModule,
  NbSelectModule, NbListModule, NbProgressBarModule, NbContextMenuModule, NbBadgeModule, NbCheckboxModule
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NbSidebarModule, NbButtonModule } from '@nebular/theme';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NbIconModule } from '@nebular/theme';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import {ChartsModule} from 'ng2-charts';
import { RunnersComponent } from './pages/runners-list/runners.component';
import { PreferencesComponent } from './pages/preferences/preferences.component';
import { RInputComponent } from './components/r-input/r-input.component';
import { RSelectComponent } from './components/r-select/r-select.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormElementComponent} from './components/form-element.component';
import { RCheckboxComponent } from './components/r-checkbox/r-checkbox.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    RunnersComponent,
    PreferencesComponent,
    RInputComponent,
    RSelectComponent,
    FormElementComponent,
    RCheckboxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    ChartsModule,

    // NB Modules
    NbThemeModule.forRoot({name: 'default'}),
    NbMenuModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbLayoutModule,
    NbEvaIconsModule,
    NbIconModule,
    NbButtonModule,
    NbCheckboxModule,
    NbUserModule,
    NbInputModule,
    NbFormFieldModule,
    NbCardModule,
    NbSelectModule,
    NbListModule,
    NbProgressBarModule,
    NbButtonModule,
    NbContextMenuModule,
    NbBadgeModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
